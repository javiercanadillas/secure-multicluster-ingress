export PROJECT_ID=${DEVSHELL_PROJECT_ID}
gcloud config set core/project ${PROJECT_ID}
export PROJECT_NUMBER=$(gcloud projects describe ${PROJECT_ID} --format 'value(projectNumber)')
gcloud projects add-iam-policy-binding ${PROJECT_ID} --member serviceAccount:${PROJECT_NUMBER}@cloudbuild.gserviceaccount.com --role roles/owner
gcloud builds submit --substitutions=_PROJECT_ID=${PROJECT_ID}
